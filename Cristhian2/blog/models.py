from django.db import models
from django.utils import timezone


# Create your models here.


class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
        default=timezone.now)
    published_date = models.DateTimeField(
        blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class Cabecera(models.Model):
    region = models.TextField()
    provincia = models.TextField()
    fecha = models.CharField(max_length=10)
    distrito = models.TextField()
    comunidad = models.TextField()
    sexo = models.TextField()
    direccion = models.TextField()
    nomfamilia = models.TextField()
    sectorintervencion = models.TextField()
    nrointegrantes = models.IntegerField()

    def __str__(self):
        return self.region