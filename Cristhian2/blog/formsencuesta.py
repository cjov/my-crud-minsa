from django import forms

from .models import Cabecera


class PostFormEncuesta(forms.ModelForm):
    class Meta:
        model = Cabecera
        fields = ('region', )
